package utils;
import java.util.Scanner;

public class SaisieClavier {
	
	public static int lireEntier(String inviteSaisie) {
		Scanner scan = new Scanner(System.in);
		
		int saisie; // saisie r�alis�e par l'utilisateur
		
		// redemander une saisie tant que celle ci n'est pas numerique
		// TODO securiser la saisie
		
		System.out.println(inviteSaisie);
		while (!scan.hasNextInt()) { // tant que la future saisie n'est pas un int
			System.out.println("un entier on t'a dit ! "); // message d'erreur
			System.out.println(inviteSaisie);
			scan.next(); // evecue la saisie invalide
		}
		
		saisie = scan.nextInt(); // � ce niveau je prend la saisie valide
		
			//System.out.println(inviteSaisie);
			//saisie = scan.nextInt();
		
		return saisie;
	}

}

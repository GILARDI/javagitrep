import utils.Menu;

public class Lanceur {

	public static void main(String[] args) {
		/*
		
		// une Machine a cafe pour moi :
		
		// un monnayeur
		
		// un distrib de boissons
		Entrepot distridistributeurBoisson = new Entrepot();
		distridistributeurBoisson.ajouterItem(new Boisson(0, "caf�"				, 60, 10));
		distridistributeurBoisson.ajouterItem(new Boisson(1, "th�"					, 40, 10));
		distridistributeurBoisson.ajouterItem(new Boisson(2, "chocolat"			, 50, 10));
		distridistributeurBoisson.ajouterItem(new Boisson(3, "caf� au lait"		, 70, 10));
		distridistributeurBoisson.ajouterItem(new Boisson(4, "velout� champignon"	, 30, 10));
		distridistributeurBoisson.ajouterItem(new Boisson(5, "eau chaude"			, 10, 10));
		
		// des menus pour faire les choix
		Menu menuBoisson = new Menu("Choix boisson", distridistributeurBoisson.getBoissons());
		int choixBoisson = 99;
		do {
			// recuperation des boissons et stocks
			menuBoisson.setChoix(distridistributeurBoisson.getBoissons());
			choixBoisson = menuBoisson.lireChoixMenu();
			// TODO traiter les mauvais choix et la sortie
			distridistributeurBoisson.getItem(choixBoisson).servirBoisson();
			System.out.println("Vous avez choisi "+distridistributeurBoisson.getItem(choixBoisson).getNom());
		} while(choixBoisson != 99);
		*/
		/*
		System.out.println("-------------------------");
		
		
		String[] pieces = {"0.10cts", "0.20cts", "0.50cts", "1�", "2�"};
		Menu menuPiece = new Menu("Ins�rez une pi�ce ", pieces);
		System.out.println("Vous avez choisi "+pieces[menuPiece.lireChoixMenu()]);
		*/
		
		System.out.println("=============================================");
		
		Stockeur<Produit> monStockBoisson = new Stockeur();
		monStockBoisson.addItem(new Produit(0, "caf�", 60, 10));
		monStockBoisson.getItem(0).addQty();
		System.out.println(monStockBoisson.getItem(0).getQte());
		monStockBoisson.getItem(0).setMarque("Gd M�re");
		System.out.println(monStockBoisson.getItem(0).getMarque());
		
		Stockable maBoisson = monStockBoisson.getItem(0);
		System.out.println((  (Produit) maBoisson    ).getMarque());
		
		Stockeur<Piece> monMonnayeur = new Stockeur();
		monMonnayeur.addItem(new Piece(0, "10cts", 10, 5));
	
		

	}

}

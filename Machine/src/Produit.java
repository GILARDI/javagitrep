/**
 * 
 */

/**
 * @author 1501224
 *
 */
public class Produit extends Stockable {

	//ATTIBUTS
	private String marque;
	
	
	/**
	 * @param id
	 * @param nom
	 */
	public Produit(int id, String nom) {
		super(id, nom);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param nom
	 * @param valeur
	 */
	public Produit(int id, String nom, int valeur) {
		super(id, nom, valeur);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param nom
	 * @param valeur
	 * @param qte
	 */
	public Produit(int id, String nom, int valeur, int qte) {
		super(id, nom, valeur, qte);
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * wxcfvbn,k;l:m!mlkwxcghwdslkjkjdfds
	 * @return
	 */
	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

}
